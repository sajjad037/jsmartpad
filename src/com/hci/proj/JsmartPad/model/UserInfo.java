package com.hci.proj.JsmartPad.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.hci.proj.JsmartPad.ConfigUtil.Enums;

/**
 * Getter Setter Class that holds the structure of user infromation
 * @author Team05_HCI
 *
 */
public class UserInfo {

	private Enums.UserType userType;
	private Date lastopen;
	private List<String> enableMenus = new ArrayList<String>();

	/**
	 * getUserType
	 * @return
	 */
	public Enums.UserType getUserType() {
		return userType;
	}

	/**
	 * setUserType
	 * @param userType
	 */
	public void setUserType(Enums.UserType userType) {
		this.userType = userType;
	}

	/**
	 * getLastopen
	 * @return
	 */
	public Date getLastopen() {
		return lastopen;
	}

	/**
	 * setLastopen
	 * @param lastopen
	 */
	public void setLastopen(Date lastopen) {
		this.lastopen = lastopen;
	}

	/**
	 * getEnableMenus
	 * @return
	 */
	public List<String> getEnableMenus() {
		return enableMenus;
	}
	
	/**
	 * setEnableMenus
	 * @param enableMenus
	 */
	public void setEnableMenus(List<String> enableMenus) {
		this.enableMenus = enableMenus;
	}
	
	/**
	 * addMenus
	 * @param menuName
	 */
	public void addMenus(String menuName) {
		if (!isAlreadyAdded(menuName)) {
			this.enableMenus.add(menuName);
		}
	}
	
	/**
	 * isAlreadyAdded
	 * @param menuName
	 * @return
	 */
	public boolean isAlreadyAdded(String menuName) {
		for (String menu : enableMenus) {
			if (menu.equals(menuName)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return "UserInfo [userType=" + userType + ", lastopen=" + lastopen + ", enableMenus=" + enableMenus + "]";
	}

}
