package com.hci.proj.JsmartPad;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * 
 * @author Team05_HCI
 *
 */
class MyFileFilter extends FileFilter {

	private String extension;
	private String description;

	/**
	 * Constructor
	 */
	public MyFileFilter() {
		setExtension(null);
		setDescription(null);
	}

	/**
	 * Constructor
	 * @param ext String
	 * @param desc String
	 */
	public MyFileFilter(final String ext, final String desc) {
		setExtension(ext);
		setDescription(desc);
	}

	/**
	 * accept
	 *  @param f File
	 *  @return boolean
	 */
	public boolean accept(File f) {
		final String filename = f.getName();

		if (f.isDirectory() || extension == null || filename.toUpperCase().endsWith(extension.toUpperCase()))
			return true;
		return false;

	}
	
	/**
	 * get Description
	 * @return String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * set Description
	 * @param desc
	 */
	public void setDescription(String desc) {
		if (desc == null)
			description = new String("All Files(*.*)");
		else
			description = new String(desc);
	}
	
	/**
	 * set Extension
	 * @param ext
	 */
	public void setExtension(String ext) {
		if (ext == null) {
			extension = null;
			return;
		}

		extension = new String(ext).toLowerCase();
		if (!ext.startsWith("."))
			extension = "." + extension;
	}
}
