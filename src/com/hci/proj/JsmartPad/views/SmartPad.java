package com.hci.proj.JsmartPad.views;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import org.apache.log4j.Logger;

import com.hci.proj.JsmartPad.FileOperation;
import com.hci.proj.JsmartPad.LookAndFeelMenu;
import com.hci.proj.JsmartPad.MenuConstants;
import com.hci.proj.JsmartPad.ConfigUtil.ApplicationStatic;
import com.hci.proj.JsmartPad.ConfigUtil.FileStorage;
import com.hci.proj.JsmartPad.subViews.FindDialog;
import com.hci.proj.JsmartPad.subViews.FontChooser;

/**
 * SmartPad
 * 
 * @author Team05_HCI
 *
 */
public class SmartPad extends BaseView implements ActionListener, MenuConstants {

	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(SmartPad.class);
	public JFrame f;
	public JTextArea ta;
	public JLabel statusBar;

	String searchString, replaceString;
	int lastSearchIndex;

	FileOperation fileHandler;
	FontChooser fontDialog = null;
	FindDialog findReplaceDialog = null;
	JColorChooser bcolorChooser = null;
	JColorChooser fcolorChooser = null;
	JDialog backgroundDialog = null;
	JDialog foregroundDialog = null;
	JMenu fileMenu, editMenu, formatMenu, viewMenu, helpMenu, allCommandMenu, viewChangeLookFeelItem;
	JMenuItem cutItem, copyItem, deleteItem, findItem, findNextItem, replaceItem, gotoItem, selectAllItem, fileNewItem,
			fileOpenItem, fileSaveItem, fileSaveAsItem, filePageSetupItem, filePrintItem, fileExitItem, editUndoItem,
			editPasteItem, editTimeDateItem, formatWordWrapItem, formatFontItem, formatForegroundItem,
			formatBackgroundItem, viewStatusBarItem, helpHelpTopicItem, helpAboutNotepadItem;

	/**
	 * Constructor - SmartPad
	 */
	SmartPad() {
		JFrame.setDefaultLookAndFeelDecorated(false);
		f = new JFrame(ApplicationStatic.TEMP_FILE_NAME + " - " + ApplicationStatic.APPLICATION_NAME);

		InputStream imageInputStream = getClass().getResourceAsStream("/smartPadIcon.png");
		BufferedImage bufferedImage = null;
		try {
			bufferedImage = ImageIO.read(imageInputStream);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		f.setIconImage(bufferedImage);

		ta = new JTextArea(30, 60);
		statusBar = new JLabel("||       Ln 1, Col 1  ", JLabel.RIGHT);
		f.getContentPane().add(new JScrollPane(ta), BorderLayout.CENTER);
		f.getContentPane().add(statusBar, BorderLayout.SOUTH);
		f.getContentPane().add(new JLabel("  "), BorderLayout.EAST);
		f.getContentPane().add(new JLabel("  "), BorderLayout.WEST);
		createMenuBar(f);
		// f.setSize(350,350);
		f.pack();
		// f.setLocation(100, 50);
		f.setVisible(true);
		// f.setLocation(150, 50);
		centreWindow(f);
		f.setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
		f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		fileHandler = new FileOperation(this);

		ta.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent e) {
				int lineNumber = 0, column = 0, pos = 0;

				try {
					pos = ta.getCaretPosition();
					lineNumber = ta.getLineOfOffset(pos);
					column = pos - ta.getLineStartOffset(lineNumber);
				} catch (Exception excp) {
				}
				if (ta.getText().length() == 0) {
					lineNumber = 0;
					column = 0;
				}
				statusBar.setText("||       Ln " + (lineNumber + 1) + ", Col " + (column + 1));
			}
		});

		DocumentListener myListener = new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				fileHandler.saved = false;
			}

			public void removeUpdate(DocumentEvent e) {
				fileHandler.saved = false;
			}

			public void insertUpdate(DocumentEvent e) {
				fileHandler.saved = false;
			}
		};
		ta.getDocument().addDocumentListener(myListener);

		WindowListener frameClose = new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				if (fileHandler.confirmSave())
					System.exit(0);
			}
		};
		f.addWindowListener(frameClose);
		//////////////////
		/*
		 * ta.append("Hello dear hello hi");
		 * ta.append("\nwho are u dear mister hello");
		 * ta.append("\nhello bye hel"); ta.append("\nHello");
		 * ta.append("\nMiss u mister hello hell"); fileHandler.saved=true;
		 */
	}

	/**
	 * goTo
	 */
	void goTo() {
		int lineNumber = 0;
		try {
			lineNumber = ta.getLineOfOffset(ta.getCaretPosition()) + 1;
			String tempStr = JOptionPane.showInputDialog(f, "Enter Line Number:", "" + lineNumber);
			if (tempStr == null) {
				return;
			}
			lineNumber = Integer.parseInt(tempStr);
			ta.setCaretPosition(ta.getLineStartOffset(lineNumber - 1));
		} catch (Exception e) {
		}
	}

	/**
	 * actionPerformed
	 * 
	 * @param ev
	 *            ActionEvent
	 */
	public void actionPerformed(ActionEvent ev) {
		String cmdText = ev.getActionCommand();
		JMenuItem mi = (JMenuItem) ev.getSource();
		JMenu menu = getMenuBarMenu(mi);
		String parentMenuName = menu.getText();

		// Responsive Menu Logic
		if (parentMenuName.equals(allCommandText)) {
			// Check Menu is not add already
			if (!ApplicationStatic.userInfo.isAlreadyAdded(cmdText)) {
				// Add it to userInfo
				String parentMenu = addMenuToUserInfo(cmdText);
				if (parentMenu != null) {
					ApplicationStatic.userInfo.addMenus(cmdText);

					// save userInfo
					FileStorage fs = new FileStorage();
					fs.saveUserInfo(ApplicationStatic.file, ApplicationStatic.userInfo);

					// Success Message
					successMessage("Command: " + cmdText + " added to menu: " + parentMenu + " successfully.", "");
				} else
					errorMessage("Failed to add Command: " + cmdText + "!", "");
			}
		}

		if (cmdText.equals(fileNew))
			fileHandler.newFile();
		else if (cmdText.equals(fileOpen))
			fileHandler.openFile();

		else if (cmdText.equals(fileSave))
			fileHandler.saveThisFile();

		else if (cmdText.equals(fileSaveAs))
			fileHandler.saveAsFile();

		else if (cmdText.equals(fileExit)) {
			if (fileHandler.confirmSave())
				System.exit(0);
		}

		else if (cmdText.equals(filePrint))
			errorMessage("Get ur printer repaired first! It seems u dont have one!", "Bad Printer");

		else if (cmdText.equals(editCut))
			ta.cut();

		else if (cmdText.equals(editCopy))
			ta.copy();

		else if (cmdText.equals(editPaste))
			ta.paste();

		else if (cmdText.equals(editDelete))
			ta.replaceSelection("");

		else if (cmdText.equals(editFind)) {
			if (SmartPad.this.ta.getText().length() == 0)
				return; // text box have no text
			if (findReplaceDialog == null)
				findReplaceDialog = new FindDialog(SmartPad.this.ta);
			findReplaceDialog.showDialog(SmartPad.this.f, true);// find
		}

		else if (cmdText.equals(editFindNext)) {
			if (SmartPad.this.ta.getText().length() == 0)
				return; // text box have no text

			if (findReplaceDialog == null)
				statusBar.setText("Nothing to search for, use Find option of Edit Menu first !!!!");
			else
				findReplaceDialog.findNextWithSelection();
		}

		else if (cmdText.equals(editReplace)) {
			if (SmartPad.this.ta.getText().length() == 0)
				return; // text box have no text

			if (findReplaceDialog == null)
				findReplaceDialog = new FindDialog(SmartPad.this.ta);
			findReplaceDialog.showDialog(SmartPad.this.f, false);// replace
		}

		else if (cmdText.equals(editGoTo)) {
			if (SmartPad.this.ta.getText().length() == 0)
				return; // text box have no text
			goTo();
		}

		else if (cmdText.equals(editSelectAll))
			ta.selectAll();

		else if (cmdText.equals(editTimeDate))
			ta.insert(new Date().toString(), ta.getSelectionStart());

		else if (cmdText.equals(formatWordWrap)) {
			JCheckBoxMenuItem temp = (JCheckBoxMenuItem) ev.getSource();
			ta.setLineWrap(temp.isSelected());
		}

		else if (cmdText.equals(formatFont)) {
			if (fontDialog == null)
				fontDialog = new FontChooser(ta.getFont());

			if (fontDialog.showDialog(SmartPad.this.f, "Choose a font"))
				SmartPad.this.ta.setFont(fontDialog.createFont());
		}

		else if (cmdText.equals(formatForeground))
			showForegroundColorDialog();

		else if (cmdText.equals(formatBackground))
			showBackgroundColorDialog();

		else if (cmdText.equals(viewStatusBar)) {
			JCheckBoxMenuItem temp = (JCheckBoxMenuItem) ev.getSource();
			statusBar.setVisible(temp.isSelected());
		}

		else if (cmdText.equals(helpAboutNotepad)) {
			infoMessage("Developed by Team 05 for HCI project!", aboutText);
		} else
			statusBar.setText("This " + cmdText + " command is yet to be implemented");

	}// action Performed

	/**
	 * show Background Color Dialog
	 */
	void showBackgroundColorDialog() {
		if (bcolorChooser == null)
			bcolorChooser = new JColorChooser();
		if (backgroundDialog == null)
			backgroundDialog = JColorChooser.createDialog(SmartPad.this.f, formatBackground, false, bcolorChooser,
					new ActionListener() {
						public void actionPerformed(ActionEvent evvv) {
							SmartPad.this.ta.setBackground(bcolorChooser.getColor());
						}
					}, null);

		backgroundDialog.setVisible(true);
	}

	/**
	 * show Foreground Color Dialog
	 */
	void showForegroundColorDialog() {
		if (fcolorChooser == null)
			fcolorChooser = new JColorChooser();
		if (foregroundDialog == null)
			foregroundDialog = JColorChooser.createDialog(SmartPad.this.f, formatForeground, false, fcolorChooser,
					new ActionListener() {
						public void actionPerformed(ActionEvent evvv) {
							SmartPad.this.ta.setForeground(fcolorChooser.getColor());
						}
					}, null);

		foregroundDialog.setVisible(true);
	}

	///////////////////////////////////
	JMenuItem createMenuItem(String s, int key, JMenu toMenu, ActionListener al) {
		JMenuItem temp = new JMenuItem(s, key);
		temp.addActionListener(al);
		toMenu.add(temp);

		return temp;
	}

	JMenuItem createMenuItem(String s, int key, JMenu toMenu, int aclKey, ActionListener al) {
		JMenuItem temp = new JMenuItem(s, key);
		temp.addActionListener(al);
		temp.setAccelerator(KeyStroke.getKeyStroke(aclKey, ActionEvent.CTRL_MASK));
		toMenu.add(temp);

		return temp;
	}

	JCheckBoxMenuItem createCheckBoxMenuItem(String s, int key, JMenu toMenu, ActionListener al) {
		JCheckBoxMenuItem temp = new JCheckBoxMenuItem(s);
		temp.setMnemonic(key);
		temp.addActionListener(al);
		temp.setSelected(false);
		toMenu.add(temp);

		return temp;
	}

	JMenu createMenu(String s, int key, JMenuBar toMenuBar) {
		JMenu temp = new JMenu(s);
		temp.setMnemonic(key);
		toMenuBar.add(temp);
		return temp;
	}

	/**
	 * create Menu Bar
	 * 
	 * @param f
	 */
	void createMenuBar(JFrame f) {
		JMenuBar mb = new JMenuBar();

		fileMenu = createMenu(fileText, KeyEvent.VK_F, mb);
		fileMenu.setVisible(false);
		editMenu = createMenu(editText, KeyEvent.VK_E, mb);
		editMenu.setVisible(false);
		formatMenu = createMenu(formatText, KeyEvent.VK_O, mb);
		formatMenu.setVisible(false);
		viewMenu = createMenu(viewText, KeyEvent.VK_V, mb);
		viewMenu.setVisible(false);
		helpMenu = createMenu(helpText, KeyEvent.VK_H, mb);
		helpMenu.setVisible(false);
		allCommandMenu = createMenu(allCommandText, KeyEvent.VK_A, mb);

		fileNewItem = createMenuItem(fileNew, KeyEvent.VK_N, fileMenu, KeyEvent.VK_N, this);
		fileNewItem.setVisible(false);
		fileOpenItem = createMenuItem(fileOpen, KeyEvent.VK_O, fileMenu, KeyEvent.VK_O, this);
		fileOpenItem.setVisible(false);
		fileSaveItem = createMenuItem(fileSave, KeyEvent.VK_S, fileMenu, KeyEvent.VK_S, this);
		fileSaveItem.setVisible(false);
		fileSaveAsItem = createMenuItem(fileSaveAs, KeyEvent.VK_A, fileMenu, this);
		fileSaveAsItem.setVisible(false);
		fileMenu.addSeparator();
		filePageSetupItem = createMenuItem(filePageSetup, KeyEvent.VK_U, fileMenu, this);
		filePageSetupItem.setEnabled(false);
		filePrintItem = createMenuItem(filePrint, KeyEvent.VK_P, fileMenu, KeyEvent.VK_P, this);
		filePrintItem.setVisible(false);
		fileMenu.addSeparator();
		fileExitItem = createMenuItem(fileExit, KeyEvent.VK_X, fileMenu, this);
		fileExitItem.setVisible(false);

		editUndoItem = createMenuItem(editUndo, KeyEvent.VK_U, editMenu, KeyEvent.VK_Z, this);
		editUndoItem.setEnabled(false);
		editMenu.addSeparator();
		cutItem = createMenuItem(editCut, KeyEvent.VK_T, editMenu, KeyEvent.VK_X, this);
		cutItem.setVisible(false);
		copyItem = createMenuItem(editCopy, KeyEvent.VK_C, editMenu, KeyEvent.VK_C, this);
		copyItem.setVisible(false);
		editPasteItem = createMenuItem(editPaste, KeyEvent.VK_P, editMenu, KeyEvent.VK_V, this);
		editPasteItem.setVisible(false);
		deleteItem = createMenuItem(editDelete, KeyEvent.VK_L, editMenu, this);
		deleteItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
		editMenu.addSeparator();
		deleteItem.setVisible(false);
		findItem = createMenuItem(editFind, KeyEvent.VK_F, editMenu, KeyEvent.VK_F, this);
		findItem.setVisible(false);
		findNextItem = createMenuItem(editFindNext, KeyEvent.VK_N, editMenu, this);
		findNextItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
		findNextItem.setVisible(false);
		replaceItem = createMenuItem(editReplace, KeyEvent.VK_R, editMenu, KeyEvent.VK_H, this);
		replaceItem.setVisible(false);
		gotoItem = createMenuItem(editGoTo, KeyEvent.VK_G, editMenu, KeyEvent.VK_G, this);
		gotoItem.setVisible(false);
		editMenu.addSeparator();
		selectAllItem = createMenuItem(editSelectAll, KeyEvent.VK_A, editMenu, KeyEvent.VK_A, this);
		selectAllItem.setVisible(false);
		editTimeDateItem = createMenuItem(editTimeDate, KeyEvent.VK_D, editMenu, this);
		editTimeDateItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
		editTimeDateItem.setVisible(false);

		formatWordWrapItem = createCheckBoxMenuItem(formatWordWrap, KeyEvent.VK_W, formatMenu, this);
		formatWordWrapItem.setVisible(false);
		formatFontItem = createMenuItem(formatFont, KeyEvent.VK_F, formatMenu, this);
		formatFontItem.setVisible(false);
		formatMenu.addSeparator();
		formatForegroundItem = createMenuItem(formatForeground, KeyEvent.VK_T, formatMenu, this);
		formatForegroundItem.setVisible(false);
		formatBackgroundItem = createMenuItem(formatBackground, KeyEvent.VK_P, formatMenu, this);
		formatBackgroundItem.setVisible(false);

		viewStatusBarItem = createCheckBoxMenuItem(viewStatusBar, KeyEvent.VK_S, viewMenu, this);
		viewStatusBarItem.setSelected(true);
		viewStatusBarItem.setVisible(false);
		/************
		 * For Look and Feel, May not work properly on different operating
		 * environment
		 ***/
		viewChangeLookFeelItem = LookAndFeelMenu.createLookAndFeelMenuItem(viewMenu, this.f);
		viewChangeLookFeelItem.setVisible(false);

		helpHelpTopicItem = createMenuItem(helpHelpTopic, KeyEvent.VK_H, helpMenu, this);
		helpHelpTopicItem.setEnabled(false);
		helpMenu.addSeparator();
		helpAboutNotepadItem = createMenuItem(helpAboutNotepad, KeyEvent.VK_A, helpMenu, this);
		helpAboutNotepadItem.setVisible(false);

		createCheckBoxMenuItem(formatWordWrap, KeyEvent.VK_W, allCommandMenu, this);
		createMenuItem(formatFont, KeyEvent.VK_F, allCommandMenu, this);
		allCommandMenu.addSeparator();
		createMenuItem(formatForeground, KeyEvent.VK_T, allCommandMenu, this);
		createMenuItem(formatBackground, KeyEvent.VK_P, allCommandMenu, this);
		allCommandMenu.addSeparator();
		createMenuItem(helpAboutNotepad, KeyEvent.VK_A, allCommandMenu, this);

		for (String menuName : ApplicationStatic.userInfo.getEnableMenus()) {
			enableMenus(menuName);
		}

		// shift to the right
		mb.add(Box.createGlue());
		InputStream imageInputStream = getClass()
				.getResourceAsStream("/" + ApplicationStatic.userInfo.getUserType().toString() + ".png");
		BufferedImage bufferedImage = null;
		try {
			bufferedImage = ImageIO.read(imageInputStream);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		JLabel label = new JLabel(new ImageIcon(bufferedImage));
		label.setToolTipText(ApplicationStatic.userInfo.getUserType().toString());
		mb.add(label);

		MenuListener editMenuListener = new MenuListener() {
			public void menuSelected(MenuEvent evvvv) {
				if (SmartPad.this.ta.getText().length() == 0) {
					findItem.setEnabled(false);
					findNextItem.setEnabled(false);
					replaceItem.setEnabled(false);
					selectAllItem.setEnabled(false);
					gotoItem.setEnabled(false);
				} else {
					findItem.setEnabled(true);
					findNextItem.setEnabled(true);
					replaceItem.setEnabled(true);
					selectAllItem.setEnabled(true);
					gotoItem.setEnabled(true);
				}
				if (SmartPad.this.ta.getSelectionStart() == ta.getSelectionEnd()) {
					cutItem.setEnabled(false);
					copyItem.setEnabled(false);
					deleteItem.setEnabled(false);
				} else {
					cutItem.setEnabled(true);
					copyItem.setEnabled(true);
					deleteItem.setEnabled(true);
				}
			}

			public void menuDeselected(MenuEvent evvvv) {
			}

			public void menuCanceled(MenuEvent evvvv) {
			}
		};
		editMenu.addMenuListener(editMenuListener);
		f.setJMenuBar(mb);
	}

	/**
	 * enable Menus
	 * 
	 * @param menuName
	 *            String
	 */
	private void enableMenus(String menuName) {
		/******* Top Menus ***********/
		if (menuName.equals(fileText)) {
			fileMenu.setVisible(true);
		} else if (menuName.equals(editText)) {
			editMenu.setVisible(true);
		} else if (menuName.equals(formatText)) {
			formatMenu.setVisible(true);
		} else if (menuName.equals(viewText)) {
			viewMenu.setVisible(true);
		} else if (menuName.equals(helpText)) {
			helpMenu.setVisible(true);
		}

		/******* File Menu ***********/
		else if (menuName.equals(fileNew)) {
			if (fileMenu.isVisible()) {
				fileNewItem.setVisible(true);
			}
		} else if (menuName.equals(fileOpen)) {
			if (fileMenu.isVisible()) {
				fileOpenItem.setVisible(true);
			}
		} else if (menuName.equals(fileSave)) {
			if (fileMenu.isVisible()) {
				fileSaveItem.setVisible(true);
			}
		} else if (menuName.equals(fileSaveAs)) {
			if (fileMenu.isVisible()) {
				fileSaveAsItem.setVisible(true);
			}
		} else if (menuName.equals(filePageSetup)) {
			if (fileMenu.isVisible()) {
				filePageSetupItem.setVisible(true);
			}
		} else if (menuName.equals(filePrint)) {
			if (fileMenu.isVisible()) {
				filePrintItem.setVisible(true);
			}
		} else if (menuName.equals(fileExit)) {
			if (fileMenu.isVisible()) {
				fileExitItem.setVisible(true);
			}
		}

		/******* Edit Menu ***********/
		else if (menuName.equals(editUndo)) {
			if (editMenu.isVisible()) {
				editUndoItem.setVisible(true);
			}
		} else if (menuName.equals(editCut)) {
			if (editMenu.isVisible()) {
				cutItem.setVisible(true);
			}
		} else if (menuName.equals(editCut)) {
			if (editMenu.isVisible()) {
				cutItem.setVisible(true);
			}
		} else if (menuName.equals(editCopy)) {
			if (editMenu.isVisible()) {
				copyItem.setVisible(true);
			}
		} else if (menuName.equals(editPaste)) {
			if (editMenu.isVisible()) {
				editPasteItem.setVisible(true);
			}
		} else if (menuName.equals(editDelete)) {
			if (editMenu.isVisible()) {
				deleteItem.setVisible(true);
			}
		} else if (menuName.equals(editFind)) {
			if (editMenu.isVisible()) {
				findItem.setVisible(true);
			}
		} else if (menuName.equals(editFind)) {
			if (editMenu.isVisible()) {
				findItem.setVisible(true);
			}
		} else if (menuName.equals(editFindNext)) {
			if (editMenu.isVisible()) {
				findNextItem.setVisible(true);
			}
		} else if (menuName.equals(editReplace)) {
			if (editMenu.isVisible()) {
				replaceItem.setVisible(true);
			}
		} else if (menuName.equals(editGoTo)) {
			if (editMenu.isVisible()) {
				gotoItem.setVisible(true);
			}
		} else if (menuName.equals(editGoTo)) {
			if (editMenu.isVisible()) {
				gotoItem.setVisible(true);
			}
		} else if (menuName.equals(editSelectAll)) {
			if (editMenu.isVisible()) {
				selectAllItem.setVisible(true);
			}
		} else if (menuName.equals(editSelectAll)) {
			if (editMenu.isVisible()) {
				selectAllItem.setVisible(true);
			}
		} else if (menuName.equals(editTimeDate)) {
			if (editMenu.isVisible()) {
				editTimeDateItem.setVisible(true);
			}
		}

		/******* Format Menu ***********/
		else if (menuName.equals(formatWordWrap)) {
			if (formatMenu.isVisible()) {
				formatWordWrapItem.setVisible(true);
			}
		} else if (menuName.equals(formatFont)) {
			if (formatMenu.isVisible()) {
				formatFontItem.setVisible(true);
			}
		} else if (menuName.equals(formatForeground)) {
			if (formatMenu.isVisible()) {
				formatForegroundItem.setVisible(true);
			}
		} else if (menuName.equals(formatBackground)) {
			if (formatMenu.isVisible()) {
				formatBackgroundItem.setVisible(true);
			}
		}

		/******* View Menu ***********/
		else if (menuName.equals(viewStatusBar)) {
			if (viewMenu.isVisible()) {
				viewStatusBarItem.setVisible(true);
			}
		} else if (menuName.equals(viewChangeLookFeel)) {
			if (viewMenu.isVisible()) {
				viewChangeLookFeelItem.setVisible(true);
			}
		}

		/******* Help Menu ***********/
		else if (menuName.equals(helpHelpTopic)) {
			if (helpMenu.isVisible()) {
				helpHelpTopicItem.setVisible(true);
			}
		} else if (menuName.equals(helpAboutNotepad)) {
			if (helpMenu.isVisible()) {
				helpAboutNotepadItem.setVisible(true);
			}
		}
	}

	/**
	 * add Menu To UserInfo
	 * 
	 * @param menuName
	 * @return
	 */
	private String addMenuToUserInfo(String menuName) {

		String parentMenu = "";

		/******* Format Menu ***********/
		if (menuName.equals(formatWordWrap) || menuName.equals(formatFont) || menuName.equals(formatForeground)
				|| menuName.equals(formatBackground)) {

			parentMenu = formatText;
			// Check Parent menu is added
			if (!ApplicationStatic.userInfo.isAlreadyAdded(formatText)) {
				ApplicationStatic.userInfo.addMenus(formatText);
				formatMenu.setVisible(true);
			}

			if (menuName.equals(formatWordWrap)) {
				if (formatMenu.isVisible()) {
					formatWordWrapItem.setVisible(true);
					// userInfo.addMenus(formatWordWrap);
				}
			} else if (menuName.equals(formatFont)) {
				if (formatMenu.isVisible()) {
					formatFontItem.setVisible(true);
					// userInfo.addMenus(formatFont);
				}
			} else if (menuName.equals(formatForeground)) {
				if (formatMenu.isVisible()) {
					formatForegroundItem.setVisible(true);
					// userInfo.addMenus(formatForeground);
				}
			} else if (menuName.equals(formatBackground)) {
				if (formatMenu.isVisible()) {
					formatBackgroundItem.setVisible(true);
					// userInfo.addMenus(formatBackground);
				}
			}
		}

		/******* Help Menu ***********/
		else if (menuName.equals(helpHelpTopic) || menuName.equals(helpAboutNotepad)) {

			parentMenu = helpText;
			// Check Parent menu is added
			if (!ApplicationStatic.userInfo.isAlreadyAdded(helpText)) {
				ApplicationStatic.userInfo.addMenus(helpText);
				helpMenu.setVisible(true);
			}

			if (menuName.equals(helpAboutNotepad)) {
				if (helpMenu.isVisible()) {
					helpAboutNotepadItem.setVisible(true);
					// userInfo.addMenus(helpAboutNotepad);
				}
			}
		}

		return parentMenu;

	}

	/**
	 * get Menu Item's Parent Menu
	 * 
	 * @param item
	 * @return
	 */
	private JMenu getMenuBarMenu(JMenuItem item) {
		JMenu menu = null;

		while (menu == null) {
			JPopupMenu popup = (JPopupMenu) item.getParent();
			item = (JMenuItem) popup.getInvoker();

			if (!(item.getParent() instanceof JPopupMenu))
				menu = (JMenu) item;
		}

		return menu;
	}

}
