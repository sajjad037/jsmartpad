package com.hci.proj.JsmartPad.views;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import com.hci.proj.JsmartPad.MenuConstants;
import com.hci.proj.JsmartPad.ConfigUtil.ApplicationStatic;
import com.hci.proj.JsmartPad.ConfigUtil.Enums;
import com.hci.proj.JsmartPad.ConfigUtil.FileStorage;
import com.hci.proj.JsmartPad.model.UserInfo;

/**
 * Select User Type Screen
 * 
 * @author Team05_HCI
 *
 */
public class SelectUserType extends BaseView implements MenuConstants {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Constructor - SelectUserType.
	 */
	public SelectUserType() {
		setTitle("User Type");
		setType(Type.POPUP);
		JFrame.setDefaultLookAndFeelDecorated(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage("resource\\smartPadIcon.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 272);
		setResizable(false);
		centreWindow(this);
		contentPane = new JPanel();
		// contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel selectUsrTypPannel = new JPanel();
		selectUsrTypPannel.setLayout(null);
		selectUsrTypPannel.setBorder(BorderFactory.createTitledBorder("Select User Type"));
		selectUsrTypPannel.setBounds(43, 21, 358, 168);
		contentPane.add(selectUsrTypPannel);

		JRadioButton rdbtnNovice = new JRadioButton(Enums.UserType.Novice.toString());
		rdbtnNovice.setBounds(106, 24, 82, 23);
		selectUsrTypPannel.add(rdbtnNovice);

		JRadioButton rdbtnAverage = new JRadioButton(Enums.UserType.Average.toString());
		rdbtnAverage.setBounds(106, 71, 82, 23);
		selectUsrTypPannel.add(rdbtnAverage);

		JRadioButton rdbtnExpert = new JRadioButton(Enums.UserType.Expert.toString());
		rdbtnExpert.setBounds(106, 118, 82, 23);
		selectUsrTypPannel.add(rdbtnExpert);

		ButtonGroup bG = new ButtonGroup();
		bG.add(rdbtnNovice);
		bG.add(rdbtnAverage);
		bG.add(rdbtnExpert);

		InputStream imageInputStream = getClass().getResourceAsStream("/novice.png");
		BufferedImage bufferedImage = null;
		try {
			bufferedImage = ImageIO.read(imageInputStream);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		JLabel lbImgNovice = new JLabel(new ImageIcon(bufferedImage));
		lbImgNovice.setBounds(183, 17, 46, 36);

		selectUsrTypPannel.add(lbImgNovice);

		imageInputStream = getClass().getResourceAsStream("/average.png");
		bufferedImage = null;
		try {
			bufferedImage = ImageIO.read(imageInputStream);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		JLabel lbImgAverage = new JLabel(new ImageIcon(bufferedImage));
		lbImgAverage.setBounds(183, 64, 46, 36);
		selectUsrTypPannel.add(lbImgAverage);

		imageInputStream = getClass().getResourceAsStream("/expert.png");
		bufferedImage = null;
		try {
			bufferedImage = ImageIO.read(imageInputStream);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		JLabel lbImgExpert = new JLabel(new ImageIcon(bufferedImage));
		lbImgExpert.setBounds(183, 111, 46, 36);
		selectUsrTypPannel.add(lbImgExpert);

		JButton btnNext = new JButton("Next");
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String userType = getSelectedButtonText(bG);

				if (userType != null) {
					Enums.UserType ut = Enums.UserType.valueOf(userType);
					ApplicationStatic.userInfo = new UserInfo();
					ApplicationStatic.userInfo.setUserType(ut);
					List<String> enableMenus = null;
					switch (ut) {
					case Novice:
						enableMenus = new ArrayList<String>();
						enableMenus.add(fileText);
						enableMenus.add(editText);
						enableMenus.add(viewText);
						enableMenus.add(allCommandText);

						enableMenus.add(fileNew);
						enableMenus.add(fileOpen);
						enableMenus.add(fileSave);
						enableMenus.add(fileSaveAs);
						enableMenus.add(filePageSetup);
						enableMenus.add(filePrint);
						enableMenus.add(fileExit);

						enableMenus.add(editUndo);
						enableMenus.add(editCut);
						enableMenus.add(editCopy);
						enableMenus.add(editPaste);
						enableMenus.add(editDelete);
						enableMenus.add(editFind);
						enableMenus.add(editFindNext);
						enableMenus.add(editReplace);
						enableMenus.add(editGoTo);
						enableMenus.add(editSelectAll);
						enableMenus.add(editTimeDate);

						enableMenus.add(viewStatusBar);
						enableMenus.add(viewChangeLookFeel);

						ApplicationStatic.userInfo.setEnableMenus(enableMenus);

						break;

					case Average:
						enableMenus = new ArrayList<String>();
						enableMenus.add(fileText);
						enableMenus.add(editText);
						enableMenus.add(formatText);
						enableMenus.add(viewText);
						enableMenus.add(allCommandText);

						enableMenus.add(fileNew);
						enableMenus.add(fileOpen);
						enableMenus.add(fileSave);
						enableMenus.add(fileSaveAs);
						enableMenus.add(filePageSetup);
						enableMenus.add(filePrint);
						enableMenus.add(fileExit);

						enableMenus.add(editUndo);
						enableMenus.add(editCut);
						enableMenus.add(editCopy);
						enableMenus.add(editPaste);
						enableMenus.add(editDelete);
						enableMenus.add(editFind);
						enableMenus.add(editFindNext);
						enableMenus.add(editReplace);
						enableMenus.add(editGoTo);
						enableMenus.add(editSelectAll);
						enableMenus.add(editTimeDate);

						enableMenus.add(formatFont);
						enableMenus.add(formatWordWrap);
						enableMenus.add(formatForeground);
						enableMenus.add(formatBackground);

						enableMenus.add(viewStatusBar);
						enableMenus.add(viewChangeLookFeel);

						ApplicationStatic.userInfo.setEnableMenus(enableMenus);
						break;

					case Expert:
						enableMenus = new ArrayList<String>();
						enableMenus.add(fileText);
						enableMenus.add(editText);
						enableMenus.add(formatText);
						enableMenus.add(viewText);
						enableMenus.add(helpText);
						enableMenus.add(allCommandText);

						enableMenus.add(fileNew);
						enableMenus.add(fileOpen);
						enableMenus.add(fileSave);
						enableMenus.add(fileSaveAs);
						enableMenus.add(filePageSetup);
						enableMenus.add(filePrint);
						enableMenus.add(fileExit);

						enableMenus.add(editUndo);
						enableMenus.add(editCut);
						enableMenus.add(editCopy);
						enableMenus.add(editPaste);
						enableMenus.add(editDelete);
						enableMenus.add(editFind);
						enableMenus.add(editFindNext);
						enableMenus.add(editReplace);
						enableMenus.add(editGoTo);
						enableMenus.add(editSelectAll);
						enableMenus.add(editTimeDate);

						enableMenus.add(formatFont);
						enableMenus.add(formatWordWrap);
						enableMenus.add(formatForeground);
						enableMenus.add(formatBackground);

						enableMenus.add(viewStatusBar);
						enableMenus.add(viewChangeLookFeel);

						enableMenus.add(helpHelpTopic);
						enableMenus.add(helpAboutNotepad);

						ApplicationStatic.userInfo.setEnableMenus(enableMenus);

						break;

					default:
						break;
					}

					// Save to file
					FileStorage fs = new FileStorage();
					fs.saveUserInfo(ApplicationStatic.file, ApplicationStatic.userInfo);

					// Move to SmartPad
					moveToSmartPad(SelectUserType.this);
				} else {
					errorMessage("Please select a User Type First!", "");
				}
			}
		});
		btnNext.setBounds(312, 200, 89, 23);
		contentPane.add(btnNext);

	}

	/**
	 * Get Selected User type Text
	 * 
	 * @param buttonGroup
	 * @return String
	 */
	public String getSelectedButtonText(ButtonGroup buttonGroup) {
		for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
			AbstractButton button = buttons.nextElement();

			if (button.isSelected()) {
				return button.getText();
			}
		}

		return null;
	}

}
