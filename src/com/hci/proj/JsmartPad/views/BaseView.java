package com.hci.proj.JsmartPad.views;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.text.ParseException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.hci.proj.JsmartPad.ConfigUtil.ApplicationStatic;

/**
 * BaseView Class is the base or parent class for all JFrame
 * @author Team05_HCI
 *
 */
public class BaseView extends JFrame {
	private static final long serialVersionUID = 1L;

	/**
	 * Set window to user's screen center position.
	 * 
	 * @param frame
	 */
	protected void centreWindow(JFrame frame) {
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
		int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
		frame.setLocation(x, y);
	}

	/**
	 * Move to, move from source window to destination.
	 * 
	 * @param currentWindow
	 * @param newWindow
	 * @param closeWindow
	 */
	private void moveTo(JFrame currentWindow, JFrame newWindow, boolean closeWindow) {
		if (closeWindow) {
			currentWindow.dispose();
		}
	}

	/**
	 * Move to File Input screen.
	 * 
	 * @param currentWindow
	 */
	protected void moveToSmartPad(JFrame currentWindow) {
		moveTo(currentWindow, new SmartPad(), true);
	}

	/**
	 * Move to Charging DataTable screen.
	 * 
	 * @param currentWindow
	 * @throws ParseException
	 */
	protected void moveToUserType(JFrame currentWindow) throws ParseException {
		moveTo(currentWindow, new SelectUserType(), true);
	}

	/**
	 * Information message pop up.
	 * 
	 * @param infoMessage
	 * @param titleBar
	 */
	protected void infoMessage(String infoMessage, String titleBar) {
		JOptionPane.showMessageDialog(null, infoMessage, ApplicationStatic.MESSAGE_TAG + titleBar,
				JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Error message pop up.
	 * 
	 * @param errorMessage
	 * @param titleBar
	 */
	protected static void errorMessage(String errorMessage, String titleBar) {
		JOptionPane.showMessageDialog(null, errorMessage, ApplicationStatic.MESSAGE_TAG + titleBar,
				JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Warning message pop up.
	 * 
	 * @param warningMessage
	 * @param titleBar
	 */
	protected void warningMessage(String warningMessage, String titleBar) {
		JOptionPane.showMessageDialog(null, warningMessage, ApplicationStatic.MESSAGE_TAG + titleBar,
				JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * Success message pop up.
	 * 
	 * @param successMessage
	 * @param titleBar
	 */
	protected void successMessage(String successMessage, String titleBar) {
		ImageIcon icon = new ImageIcon(Toolkit.getDefaultToolkit()
				.getImage("resource\\success.png"));
		JOptionPane.showMessageDialog(null, successMessage, ApplicationStatic.MESSAGE_TAG + titleBar,
				JOptionPane.INFORMATION_MESSAGE, icon);
	}
	
	/**
	 * Plain message pop up.
	 * 
	 * @param plainMessage
	 * @param titleBar
	 */
	protected void plainMessage(String plainMessage, String titleBar) {
		JOptionPane.showMessageDialog(null, plainMessage, ApplicationStatic.MESSAGE_TAG + titleBar,
				JOptionPane.PLAIN_MESSAGE);
	}

}