package com.hci.proj.JsmartPad.views;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.hci.proj.JsmartPad.ConfigUtil.ApplicationStatic;
import com.hci.proj.JsmartPad.ConfigUtil.FileStorage;
import com.hci.proj.JsmartPad.model.UserInfo;

/**
 * It is driver class which start the Application
 * @author Team05_HCI
 *
 */
class MainClass {
	final static Logger logger = Logger.getLogger(MainClass.class);
	
	/**
	 * Main Method
	 * @param s String[]
	 * @throws IOException
	 */
	public static void main(String[] s) throws IOException {
		logger.info("main ...");
		String userHome = System.getProperty("user.home");
		String path = userHome + "\\" + ApplicationStatic.CONFIGURATION_FOLDER_NAME + "\\" + ApplicationStatic.CONFIGURATION_FILE_NAME;

		ApplicationStatic.file = new File(path);
		if (ApplicationStatic.file.exists()) {

			// Read file
			FileStorage fs = new FileStorage();
			ApplicationStatic.userInfo = fs.loadUserInfo(ApplicationStatic.file);
			new SmartPad();

		} else {
			// Create Directory
			path = userHome + "\\" + ApplicationStatic.CONFIGURATION_FOLDER_NAME + "\\";
			ApplicationStatic.file = new File(path);
			ApplicationStatic.file.mkdirs();

			// Add File to Path
			path = userHome + "\\" + ApplicationStatic.CONFIGURATION_FOLDER_NAME + "\\" + ApplicationStatic.CONFIGURATION_FILE_NAME;
			ApplicationStatic.file = new File(path);
			ApplicationStatic.userInfo = new UserInfo();
			SelectUserType us = new SelectUserType();
			us.setVisible(true);

		}
	}
}