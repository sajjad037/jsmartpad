package com.hci.proj.JsmartPad.ConfigUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.google.gson.Gson;
import com.hci.proj.JsmartPad.model.UserInfo;

/**
 * This class is for saving the user info
 * 
 * @author Team05_HCI
 * 
 */
public class FileStorage {

	/**
	 * this method saves the user info
	 * 
	 * @param new_file
	 *            creates a new file object
	 * @param new_userInfo
	 *            creates a new user info
	 * @return String message
	 */
	public String saveUserInfo(File new_file, UserInfo new_userInfo) {
		String fileContent = getJsonFromObject(new_userInfo);
		fileContent = (new MiscellaneousHelper()).EncodeBase64(fileContent);
		String filePath = new_file.getPath();
		if (!filePath.endsWith(".txt"))
			filePath += ".txt";
		try {
			FileWriter fileWriter = new FileWriter(filePath);
			fileWriter.write(fileContent);
			fileWriter.flush();
			fileWriter.close();
			return "SUCCESS";
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR : " + e.getMessage();
		}
	}

	/**
	 * this method read up the user info
	 * 
	 * @param new_file
	 *            new file object
	 * @return UserInfo object of UserInfo
	 */
	public UserInfo loadUserInfo(File new_file) {

		String fileContent = "";
		try {
			fileContent = new String(Files.readAllBytes(Paths.get(new_file.getPath())));
			fileContent = (new MiscellaneousHelper()).DecodeBase64(fileContent);
			return (UserInfo) getObjectFromJson(fileContent, UserInfo.class);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * this method gets json from object
	 * 
	 * @param new_object
	 *            new object
	 * @return json converts gson to json and returns it
	 */
	public String getJsonFromObject(Object new_object) {
		Gson gson = new Gson();
		return gson.toJson(new_object);
	}

	/**
	 * this methods gets object from a json
	 * 
	 * @param new_jsonString
	 *            json string object
	 * @param new_class
	 *            new clas
	 * @return object object from json
	 */
	public Object getObjectFromJson(String new_jsonString, Class<?> new_class) {
		Gson gson = new Gson();
		return gson.fromJson(new_jsonString, new_class);
	}

}
