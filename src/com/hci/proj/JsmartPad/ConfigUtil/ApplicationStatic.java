package com.hci.proj.JsmartPad.ConfigUtil;

import java.io.File;

import com.hci.proj.JsmartPad.model.UserInfo;

/**
 * Manage static Content
 * @author  Team05_HCI
 *
 */
public class ApplicationStatic {

	public final static String APPLICATION_NAME = "JSmartPad";
	public final static String APPLICATION_TITLE = "Notepad - JSmartPad";
	public static final String MESSAGE_TAG = "JSmartPad - Message";
	public final static String TEMP_FILE_NAME = "Untitled";
	public final static String CONFIGURATION_FILE_NAME = "JSmartPad.txt";
	public final static String CONFIGURATION_FOLDER_NAME = "JSmartPad";

	public static UserInfo userInfo = null;
	public static File file = null;
}
