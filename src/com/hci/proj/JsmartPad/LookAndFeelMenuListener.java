package com.hci.proj.JsmartPad;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 * Handle the Click events or ActionListner 
 * @author Team05_HCI
 *
 */
class LookAndFeelMenuListener implements ActionListener {
	String classname;
	Component jf;

	/**
	 * Constructor LookAndFeelMenuListener
	 * @param cln String
	 * @param jf Component
	 */
	LookAndFeelMenuListener(String cln, Component jf) {
		this.jf = jf;
		classname = new String(cln);
	}

	public void actionPerformed(ActionEvent ev) {
		try {
			UIManager.setLookAndFeel(classname);
			SwingUtilities.updateComponentTreeUI(jf);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
