package com.hci.proj.JsmartPad;

import java.awt.Component;

import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.UIManager;

/**
 * Construct and manage loopAndFeelMenu Items.
 * @author SajjadAshrafCan
 *
 */
public class LookAndFeelMenu implements MenuConstants {

	/**
	 * create Look And Feel MenuItem
	 * @param jmenu JMenu
	 * @param cmp Component
	 * @return JMenu
	 */
	public static JMenu createLookAndFeelMenuItem(JMenu jmenu, Component cmp) {
		final UIManager.LookAndFeelInfo[] infos = UIManager.getInstalledLookAndFeels();

		JRadioButtonMenuItem rbm[] = new JRadioButtonMenuItem[infos.length];
		ButtonGroup bg = new ButtonGroup();
		JMenu tmp = new JMenu(viewChangeLookFeel);
		tmp.setMnemonic('C');
		for (int i = 0; i < infos.length; i++) {
			rbm[i] = new JRadioButtonMenuItem(infos[i].getName());
			rbm[i].setMnemonic(infos[i].getName().charAt(0));
			tmp.add(rbm[i]);
			bg.add(rbm[i]);
			rbm[i].addActionListener(new LookAndFeelMenuListener(infos[i].getClassName(), cmp));
		}

		rbm[0].setSelected(true);
		jmenu.add(tmp);

		return tmp;
	}

}
