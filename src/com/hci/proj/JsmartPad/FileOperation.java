package com.hci.proj.JsmartPad;

import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.hci.proj.JsmartPad.ConfigUtil.ApplicationStatic;
import com.hci.proj.JsmartPad.views.SmartPad;

/**
 * Class which handle file related operations.
 * 
 * @author Team05_HCI
 *
 */
public class FileOperation {
	SmartPad npd;

	public boolean saved;
	boolean newFileFlag;
	String fileName;

	File fileRef;
	JFileChooser chooser;

	/**
	 * is file saved
	 * @return
	 */
	boolean isSave() {
		return saved;
	}

	/**
	 * set Save
	 * @param saved
	 */
	void setSave(boolean saved) {
		this.saved = saved;
	}

	/**
	 * get File Name
	 * @return
	 */
	String getFileName() {
		return new String(fileName);
	}

	/**
	 * set File Name
	 * @param fileName
	 */
	void setFileName(String fileName) {
		this.fileName = new String(fileName);
	}

	/**
	 * Constructor File Operation
	 * @param npd SmartPad
	 */
	public FileOperation(SmartPad npd) {
		this.npd = npd;

		saved = true;
		newFileFlag = true;
		fileName = new String("Untitled");
		fileRef = new File(fileName);
		this.npd.f.setTitle(fileName + " - " + ApplicationStatic.APPLICATION_TITLE);

		chooser = new JFileChooser();
		chooser.addChoosableFileFilter(new MyFileFilter(".java", "Java Source Files(*.java)"));
		chooser.addChoosableFileFilter(new MyFileFilter(".txt", "Text Files(*.txt)"));
		chooser.setCurrentDirectory(new File("."));

	}

	/**
	 * save File
	 * @param temp File
	 * @return
	 */
	boolean saveFile(File temp) {
		FileWriter fout = null;
		try {
			fout = new FileWriter(temp);
			fout.write(npd.ta.getText());
		} catch (IOException ioe) {
			updateStatus(temp, false);
			return false;
		} finally {
			try {
				fout.close();
			} catch (IOException excp) {
			}
		}
		updateStatus(temp, true);
		return true;
	}

	/**
	 * save This File
	 * @return
	 */
	public boolean saveThisFile() {

		if (!newFileFlag) {
			return saveFile(fileRef);
		}

		return saveAsFile();
	}

	/**
	 * save As File
	 * @return
	 */
	public boolean saveAsFile() {
		File temp = null;
		chooser.setDialogTitle("Save As...");
		chooser.setApproveButtonText("Save Now");
		chooser.setApproveButtonMnemonic(KeyEvent.VK_S);
		chooser.setApproveButtonToolTipText("Click me to save!");

		do {
			if (chooser.showSaveDialog(this.npd.f) != JFileChooser.APPROVE_OPTION)
				return false;
			temp = chooser.getSelectedFile();
			if (!temp.exists())
				break;
			if (JOptionPane.showConfirmDialog(this.npd.f,
					"<html>" + temp.getPath() + " already exists.<br>Do you want to replace it?<html>", "Save As",
					JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
				break;
		} while (true);

		return saveFile(temp);
	}

	/**
	 * open File
	 * @param temp File
	 * @return
	 */
	boolean openFile(File temp) {
		FileInputStream fin = null;
		BufferedReader din = null;

		try {
			fin = new FileInputStream(temp);
			din = new BufferedReader(new InputStreamReader(fin));
			String str = " ";
			while (str != null) {
				str = din.readLine();
				if (str == null)
					break;
				this.npd.ta.append(str + "\n");
			}

		} catch (IOException ioe) {
			updateStatus(temp, false);
			return false;
		} finally {
			try {
				din.close();
				fin.close();
			} catch (IOException excp) {
			}
		}
		updateStatus(temp, true);
		this.npd.ta.setCaretPosition(0);
		return true;
	}

	/**
	 * open File
	 */
	public void openFile() {
		if (!confirmSave())
			return;
		chooser.setDialogTitle("Open File...");
		chooser.setApproveButtonText("Open this");
		chooser.setApproveButtonMnemonic(KeyEvent.VK_O);
		chooser.setApproveButtonToolTipText("Click me to open the selected file.!");

		File temp = null;
		do {
			if (chooser.showOpenDialog(this.npd.f) != JFileChooser.APPROVE_OPTION)
				return;
			temp = chooser.getSelectedFile();

			if (temp.exists())
				break;

			JOptionPane.showMessageDialog(this.npd.f,
					"<html>" + temp.getName() + "<br>file not found.<br>"
							+ "Please verify the correct file name was given.<html>",
					"Open", JOptionPane.INFORMATION_MESSAGE);

		} while (true);

		this.npd.ta.setText("");

		if (!openFile(temp)) {
			fileName = "Untitled";
			saved = true;
			this.npd.f.setTitle(fileName + " - " + ApplicationStatic.APPLICATION_TITLE);
		}
		if (!temp.canWrite())
			newFileFlag = true;

	}

	/**
	 * update Status
	 * @param temp File
	 * @param saved boolean
	 */
	void updateStatus(File temp, boolean saved) {
		if (saved) {
			this.saved = true;
			fileName = new String(temp.getName());
			if (!temp.canWrite()) {
				fileName += "(Read only)";
				newFileFlag = true;
			}
			fileRef = temp;
			npd.f.setTitle(fileName + " - " + ApplicationStatic.APPLICATION_TITLE);
			npd.statusBar.setText("File : " + temp.getPath() + " saved/opened successfully.");
			newFileFlag = false;
		} else {
			npd.statusBar.setText("Failed to save/open : " + temp.getPath());
		}
	}
	
	/**
	 * confirm Save
	 * @return
	 */
	public boolean confirmSave() {
		String strMsg = "<html>The text in the " + fileName + " file has been changed.<br>"
				+ "Do you want to save the changes?<html>";
		if (!saved) {
			int x = JOptionPane.showConfirmDialog(this.npd.f, strMsg, ApplicationStatic.APPLICATION_TITLE,
					JOptionPane.YES_NO_CANCEL_OPTION);

			if (x == JOptionPane.CANCEL_OPTION)
				return false;
			if (x == JOptionPane.YES_OPTION && !saveAsFile())
				return false;
		}
		return true;
	}

	/**
	 * new File
	 */
	public void newFile() {
		if (!confirmSave())
			return;

		this.npd.ta.setText("");
		fileName = new String("Untitled");
		fileRef = new File(fileName);
		saved = true;
		newFileFlag = true;
		this.npd.f.setTitle(fileName + " - " + ApplicationStatic.APPLICATION_TITLE);
	}

}